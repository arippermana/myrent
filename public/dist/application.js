'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function () {
  // Init module configuration options
  var applicationModuleName = 'mean';
  var applicationModuleVendorDependencies = ['ngResource', 'ngAnimate', 'ngMessages', 'ui.router', 'ui.bootstrap', 'ui.utils', 'angularFileUpload', 'angular-multi-select', 'ngDroplet', 'multiselect-searchtree', 'ivh.treeview'];

  // Add a new vertical module
  var registerModule = function (moduleName, dependencies) {
    // Create angular module
    angular.module(moduleName, dependencies || []);

    // Add the module to the AngularJS configuration file
    angular.module(applicationModuleName).requires.push(moduleName);
  };

  return {
    applicationModuleName: applicationModuleName,
    applicationModuleVendorDependencies: applicationModuleVendorDependencies,
    registerModule: registerModule
  };
})();

'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider', '$httpProvider',
  function ($locationProvider, $httpProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');

    $httpProvider.interceptors.push('authInterceptor');
  }
]);

angular.module(ApplicationConfiguration.applicationModuleName).run(["$rootScope", "$state", "Authentication", function ($rootScope, $state, Authentication) {

  // Check authentication before changing state
  $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    if (toState.data && toState.data.roles && toState.data.roles.length > 0) {
      var allowed = false;
      toState.data.roles.forEach(function (role) {
        if (Authentication.user.roles !== undefined && Authentication.user.roles.indexOf(role) !== -1) {
          allowed = true;
          return true;
        }
      });

      if (!allowed) {
        event.preventDefault();
        if (Authentication.user !== undefined && typeof Authentication.user === 'object') {
          $state.go('forbidden');
        } else {
          $state.go('authentication.signin').then(function () {
            storePreviousState(toState, toParams);
          });
        }
      }
    }
  });

  // Record previous state
  $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    storePreviousState(fromState, fromParams);
  });

  // Store previous state
  function storePreviousState(state, params) {
    // only store this state if it shouldn't be ignored 
    if (!state.data || !state.data.ignoreState) {
      $state.previous = {
        state: state,
        params: params,
        href: $state.href(state, params)
      };
    }
  }
}]);

//Then define the init function for starting up the application
angular.element(document).ready(function () {
  //Fixing facebook bug with redirect
  if (window.location.hash && window.location.hash === '#_=_') {
    if (window.history && history.pushState) {
      window.history.pushState('', document.title, window.location.pathname);
    } else {
      // Prevent scrolling by storing the page's current scroll offset
      var scroll = {
        top: document.body.scrollTop,
        left: document.body.scrollLeft
      };
      window.location.hash = '';
      // Restore the scroll offset, should be flicker free
      document.body.scrollTop = scroll.top;
      document.body.scrollLeft = scroll.left;
    }
  }

  //Then init the app
  angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('articles');

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('chat');

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
ApplicationConfiguration.registerModule('core.admin', ['core']);
ApplicationConfiguration.registerModule('core.admin.routes', ['ui.router']);

(function (app) {
  'use strict';

  app.registerModule('goods');
}(ApplicationConfiguration));

(function (app) {
  'use strict';

  app.registerModule('master-data');
}(ApplicationConfiguration));

(function (app) {
  'use strict';

  app.registerModule('shared');
}(ApplicationConfiguration));

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users', ['core']);
ApplicationConfiguration.registerModule('users.admin', ['core.admin']);
ApplicationConfiguration.registerModule('users.admin.routes', ['core.admin.routes']);

'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
  function (Menus) {
    // Add the articles dropdown item
    Menus.addMenuItem('topbar', {
      title: 'Articles',
      state: 'articles',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    Menus.addSubMenuItem('topbar', 'articles', {
      title: 'List Articles',
      state: 'articles.list'
    });

    // Add the dropdown create item
    Menus.addSubMenuItem('topbar', 'articles', {
      title: 'Create Articles',
      state: 'articles.create',
      roles: ['user']
    });
  }
]);

'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider',
  function ($stateProvider) {
    // Articles state routing
    $stateProvider
      .state('articles', {
        abstract: true,
        url: '/articles',
        template: '<ui-view/>'
      })
      .state('articles.list', {
        url: '',
        templateUrl: 'modules/articles/client/views/list-articles.client.view.html'
      })
      .state('articles.create', {
        url: '/create',
        templateUrl: 'modules/articles/client/views/create-article.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('articles.view', {
        url: '/:articleId',
        templateUrl: 'modules/articles/client/views/view-article.client.view.html'
      })
      .state('articles.edit', {
        url: '/:articleId/edit',
        templateUrl: 'modules/articles/client/views/edit-article.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      });
  }
]);

'use strict';

// Articles controller
angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Articles',
  function ($scope, $stateParams, $location, Authentication, Articles) {
    $scope.authentication = Authentication;

    // Create new Article
    $scope.create = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'articleForm');

        return false;
      }

      // Create new Article object
      var article = new Articles({
        title: this.title,
        content: this.content
      });

      // Redirect after save
      article.$save(function (response) {
        $location.path('articles/' + response._id);

        // Clear form fields
        $scope.title = '';
        $scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Remove existing Article
    $scope.remove = function (article) {
      if (article) {
        article.$remove();

        for (var i in $scope.articles) {
          if ($scope.articles[i] === article) {
            $scope.articles.splice(i, 1);
          }
        }
      } else {
        $scope.article.$remove(function () {
          $location.path('articles');
        });
      }
    };

    // Update existing Article
    $scope.update = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'articleForm');

        return false;
      }

      var article = $scope.article;

      article.$update(function () {
        $location.path('articles/' + article._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    // Find a list of Articles
    $scope.find = function () {
      $scope.articles = Articles.query();
    };

    // Find existing Article
    $scope.findOne = function () {
      $scope.article = Articles.get({
        articleId: $stateParams.articleId
      });
    };
  }
]);

'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('articles').factory('Articles', ['$resource',
  function ($resource) {
    return $resource('api/articles/:articleId', {
      articleId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

'use strict';

// Configuring the Chat module
angular.module('chat').run(['Menus',
  function (Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', {
      title: 'Chat',
      state: 'chat'
    });
  }
]);

'use strict';

// Configure the 'chat' module routes
angular.module('chat').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('chat', {
        url: '/chat',
        templateUrl: 'modules/chat/client/views/chat.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      });
  }
]);

'use strict';

// Create the 'chat' controller
angular.module('chat').controller('ChatController', ['$scope', '$location', 'Authentication', 'Socket',
  function ($scope, $location, Authentication, Socket) {
    // Create a messages array
    $scope.messages = [];

    // If user is not signed in then redirect back home
    if (!Authentication.user) {
      $location.path('/');
    }

    // Make sure the Socket is connected
    if (!Socket.socket) {
      Socket.connect();
    }

    // Add an event listener to the 'chatMessage' event
    Socket.on('chatMessage', function (message) {
      $scope.messages.unshift(message);
    });

    // Create a controller method for sending messages
    $scope.sendMessage = function () {
      // Create a new message object
      var message = {
        text: this.messageText
      };

      // Emit a 'chatMessage' message event
      Socket.emit('chatMessage', message);

      // Clear the message text
      this.messageText = '';
    };

    // Remove the event listener when the controller instance is destroyed
    $scope.$on('$destroy', function () {
      Socket.removeListener('chatMessage');
    });
  }
]);

(function () {
  'use strict';

  angular
    .module('chat')
    .factory('chatService', chatService);

  chatService.$inject = [/*Example: '$state', '$window' */];

  function chatService(/*Example: $state, $window */) {
    // Chatservice service logic
    // ...

    // Public API
    return {
      someMethod: function () {
        return true;
      }
    };
  }

  function doSomething(){
    console.log('hi arip');
  }
})();

'use strict';

angular.module('core.admin').run(['Menus',
  function (Menus) {
    Menus.addMenuItem('topbar', {
      title: 'Admin',
      state: 'admin',
      type: 'dropdown',
      roles: ['admin']
    });
  }
]);

'use strict';

// Setting up route
angular.module('core.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin', {
        abstract: true,
        url: '/admin',
        template: '<ui-view/>',
        data: {
          roles: ['admin']
        }
      });
  }
]);

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {

    // Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function ($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });

    // Home state routing
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'modules/core/client/views/home.client.view.html'
    })
    .state('not-found', {
      url: '/not-found',
      templateUrl: 'modules/core/client/views/404.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('bad-request', {
      url: '/bad-request',
      templateUrl: 'modules/core/client/views/400.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('forbidden', {
      url: '/forbidden',
      templateUrl: 'modules/core/client/views/403.client.view.html',
      data: {
        ignoreState: true
      }
    })
    .state('aboutus', {
      url: '/aboutus',
      templateUrl: 'modules/core/client/views/about-us.client.view.html'
    })
    .state('contact', {
      url: '/contact',
      templateUrl: 'modules/core/client/views/contact.client.view.html'
    });
  }
]);

'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$state', 'Authentication', 'Menus',
  function ($scope, $state, Authentication, Menus) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');

    // Toggle the menu items
    $scope.isCollapsed = false;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };

    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);

'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', 'States',
  function ($scope, Authentication, States) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope.states=[];

    $scope.stateSearchResult = function(){
      return $scope.states;
    };

    $scope.find = function(){
      //States
      States.listStates(function(results, headers) {
        $scope.states = results;
      });
    };
  }
]);

'use strict';

/**
 * Edits by Ryan Hutchison
 * Credit: https://github.com/paulyoder/angular-bootstrap-show-errors */

angular.module('core')
  .directive('showErrors', ['$timeout', '$interpolate', function ($timeout, $interpolate) {
    var linkFn = function (scope, el, attrs, formCtrl) {
      var inputEl, inputName, inputNgEl, options, showSuccess, toggleClasses,
        initCheck = false,
        showValidationMessages = false,
        blurred = false;

      options = scope.$eval(attrs.showErrors) || {};
      showSuccess = options.showSuccess || false;
      inputEl = el[0].querySelector('.form-control[name]') || el[0].querySelector('[name]');
      inputNgEl = angular.element(inputEl);
      inputName = $interpolate(inputNgEl.attr('name') || '')(scope);

      if (!inputName) {
        throw 'show-errors element has no child input elements with a \'name\' attribute class';
      }

      var reset = function () {
        return $timeout(function () {
          el.removeClass('has-error');
          el.removeClass('has-success');
          showValidationMessages = false;
        }, 0, false);
      };

      scope.$watch(function () {
        return formCtrl[inputName] && formCtrl[inputName].$invalid;
      }, function (invalid) {
        return toggleClasses(invalid);
      });

      scope.$on('show-errors-check-validity', function (event, name) {
        if (angular.isUndefined(name) || formCtrl.$name === name) {
          initCheck = true;
          showValidationMessages = true;

          return toggleClasses(formCtrl[inputName].$invalid);
        }
      });

      scope.$on('show-errors-reset', function (event, name) {
        if (angular.isUndefined(name) || formCtrl.$name === name) {
          return reset();
        }
      });

      toggleClasses = function (invalid) {
        el.toggleClass('has-error', showValidationMessages && invalid);
        if (showSuccess) {
          return el.toggleClass('has-success', showValidationMessages && !invalid);
        }
      };
    };

    return {
      restrict: 'A',
      require: '^form',
      compile: function (elem, attrs) {
        if (attrs.showErrors.indexOf('skipFormGroupCheck') === -1) {
          if (!(elem.hasClass('form-group') || elem.hasClass('input-group'))) {
            throw 'show-errors element does not have the \'form-group\' or \'input-group\' class';
          }
        }
        return linkFn;
      }
    };
  }]);

'use strict';

angular.module('core').factory('authInterceptor', ['$q', '$injector',
  function ($q, $injector) {
    return {
      responseError: function(rejection) {
        if (!rejection.config.ignoreAuthModule) {
          switch (rejection.status) {
            case 401:
              $injector.get('$state').transitionTo('authentication.signin');
              break;
            case 403:
              $injector.get('$state').transitionTo('forbidden');
              break;
          }
        }
        // otherwise, default behaviour
        return $q.reject(rejection);
      }
    };
  }
]);

'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [
  function () {
    // Define a set of default roles
    this.defaultRoles = ['user', 'admin'];

    // Define the menus object
    this.menus = {};

    // A private function for rendering decision
    var shouldRender = function (user) {
      if (!!~this.roles.indexOf('*')) {
        return true;
      } else {
        if(!user) {
          return false;
        }
        for (var userRoleIndex in user.roles) {
          for (var roleIndex in this.roles) {
            if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
              return true;
            }
          }
        }
      }

      return false;
    };

    // Validate menu existance
    this.validateMenuExistance = function (menuId) {
      if (menuId && menuId.length) {
        if (this.menus[menuId]) {
          return true;
        } else {
          throw new Error('Menu does not exist');
        }
      } else {
        throw new Error('MenuId was not provided');
      }

      return false;
    };

    // Get the menu object by menu id
    this.getMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Return the menu object
      return this.menus[menuId];
    };

    // Add new menu object by menu id
    this.addMenu = function (menuId, options) {
      options = options || {};

      // Create the new menu
      this.menus[menuId] = {
        roles: options.roles || this.defaultRoles,
        items: options.items || [],
        shouldRender: shouldRender
      };

      // Return the menu object
      return this.menus[menuId];
    };

    // Remove existing menu object by menu id
    this.removeMenu = function (menuId) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Return the menu object
      delete this.menus[menuId];
    };

    // Add menu item object
    this.addMenuItem = function (menuId, options) {
      options = options || {};

      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Push new menu item
      this.menus[menuId].items.push({
        title: options.title || '',
        state: options.state || '',
        type: options.type || 'item',
        class: options.class,
        roles: ((options.roles === null || typeof options.roles === 'undefined') ? this.defaultRoles : options.roles),
        position: options.position || 0,
        items: [],
        shouldRender: shouldRender
      });

      // Add submenu items
      if (options.items) {
        for (var i in options.items) {
          this.addSubMenuItem(menuId, options.state, options.items[i]);
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    // Add submenu item object
    this.addSubMenuItem = function (menuId, parentItemState, options) {
      options = options || {};

      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Search for menu item
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].state === parentItemState) {
          // Push new submenu item
          this.menus[menuId].items[itemIndex].items.push({
            title: options.title || '',
            state: options.state || '',
            roles: ((options.roles === null || typeof options.roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : options.roles),
            position: options.position || 0,
            shouldRender: shouldRender
          });
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    // Remove existing menu object by menu id
    this.removeMenuItem = function (menuId, menuItemState) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        if (this.menus[menuId].items[itemIndex].state === menuItemState) {
          this.menus[menuId].items.splice(itemIndex, 1);
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    // Remove existing menu object by menu id
    this.removeSubMenuItem = function (menuId, submenuItemState) {
      // Validate that the menu exists
      this.validateMenuExistance(menuId);

      // Search for menu item to remove
      for (var itemIndex in this.menus[menuId].items) {
        for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
          if (this.menus[menuId].items[itemIndex].items[subitemIndex].state === submenuItemState) {
            this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
          }
        }
      }

      // Return the menu object
      return this.menus[menuId];
    };

    //Adding the topbar menu
    this.addMenu('topbar', {
      roles: ['*']
    });
  }
]);

'use strict';

// Create the Socket.io wrapper service
angular.module('core').service('Socket', ['Authentication', '$state', '$timeout',
  function (Authentication, $state, $timeout) {
    // Connect to Socket.io server
    this.connect = function () {
      // Connect only when authenticated
      if (Authentication.user) {
        this.socket = io();
      }
    };
    this.connect();

    // Wrap the Socket.io 'on' method
    this.on = function (eventName, callback) {
      if (this.socket) {
        this.socket.on(eventName, function (data) {
          $timeout(function () {
            callback(data);
          });
        });
      }
    };

    // Wrap the Socket.io 'emit' method
    this.emit = function (eventName, data) {
      if (this.socket) {
        this.socket.emit(eventName, data);
      }
    };

    // Wrap the Socket.io 'removeListener' method
    this.removeListener = function (eventName) {
      if (this.socket) {
        this.socket.removeListener(eventName);
      }
    };
  }
]);

(function() {
  'use strict';

  // Goods module config
  angular
    .module('goods')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    // ...
  }
})();

(function () {
  'use strict';

  //Setting up route
  angular
    .module('goods')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Goods state routing
    $stateProvider
      .state('goods-create', {
        url: '/goodsCreate',
        templateUrl: 'modules/goods/client/views/good-create.client.view.html',
        controller: 'GoodsCreateController',
        controllerAs: 'vm'
      })
      .state('good-list', {
        url: '/goodList',
        templateUrl: 'modules/goods/client/views/good-list.client.view.html',
        controller: 'GoodsListController',
        ControllerAs: 'vm'
      })
      .state('good-test', {
        url: '/goodsTest',
        templateUrl: 'modules/goods/client/views/goods-test-upload.client.view.html',
        ControllerAs: 'vm'
      })
      .state('goods-test-dragdrop', {
        url: '/goodsTestDragDrop',
        templateUrl: 'modules/goods/client/views/goods.test-droplet.client.view.html',
        controller: 'IndexController',
        controllerAs: 'vm'
      })
      .state('goods-user-profile',{
        url: '/goodsUserProfile',
        templateUrl: 'modules/goods/client/views/goods-seller-profile.client.view.html',
        controller: 'GoodsUserProfileController',
        params: {
          user: ''
        }
      });
  }
})();

'use strict';

//Goods List Controller
angular.module('goods').controller('IndexController', ['$scope', 'States', 'Districts', 'Categories', 'SubCategories', 'GoodList', '$timeout',
    function($scope, States, Districts, Categories, SubCategories, GoodList, $timeout){
  //master data initialization
      $scope.interface = {};
      $scope.uploadCount = 0;
      $scope.success = false;
      $scope.error = false;

      // Listen for when the interface has been configured.
      $scope.$on('$dropletReady', function whenDropletReady() {
        $scope.interface.allowedExtensions(['png', 'jpg', 'bmp', 'gif', 'svg', 'torrent']);
        $scope.interface.setRequestUrl('upload.html');
        $scope.interface.defineHTTPSuccess([/2.{2}/]);
        $scope.interface.useArray(false);
      });
    
      // Listen for when the files have been successfully uploaded.
      $scope.$on('$dropletSuccess', function onDropletSuccess(event, response, files) {
        $scope.uploadCount = files.length;
        $scope.success = true;
        console.log(response, files);
        $timeout(function timeout() {
          $scope.success = false;
        }, 5000);
      });
    
      // Listen for when the files have failed to upload.
      $scope.$on('$dropletError', function onDropletError(event, response) {
        $scope.error = true;
        console.log(response);
        $timeout(function timeout() {
          $scope.error = false;
        }, 5000);
      });
    }]);
'use strict';

//Goods Controller
angular.module('goods').controller('GoodsController', ['$scope', '$stateParams', '$location', 'Authentication', 'States', 'Districts', 'Categories', 'SubCategories', 'Goods',
  function ($scope, $stateParams, $location, Authentication, States, Districts, Categories, SubCategories, Goods) {
    //Master Data Initialization
    $scope.states = [];
    $scope.districts = [];
    $scope.categories = [];
    $scope.subcategories = [];

    $scope.find = function(){
      //States
      States.listStates(function(results, headers) {
        $scope.states = results;
      });

      //Districts
      Districts.listDistricts(function(results, headers){
        $scope.districts = results;
      });

      //Categories
      Categories.listCategories(function(results, headers){
        var categories = results;
        
      });

      //SubCategories
      SubCategories.listSubCategories(function(results, headers){
        $scope.subcategories = results;
      });
    };

    // Create new Good
    $scope.createGood = function () {
      $scope.error = null;

      /*if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'articleForm');

        return false;
      }*/

      // Create new Article object
      var good = new Goods({
        category: this.good.category,
        subCategory: this.good.subCategory,
        adType: this.good.type,
        adTitle: this.good.title,
        adDesc: this.good.description,
        price: this.good.price,
        state: this.good.state,
        district: this.good.district
      });

      // Redirect after save
      good.$save(function (response) {
        $location.path('articles/' + response._id);

        // Clear form fields
        //$scope.title = '';
        //$scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);
'use strict';

//Goods Controller
angular.module('goods').controller('GoodsCreateController', ['$scope', '$window', '$timeout', '$stateParams', '$location', '$filter', 'Authentication', 'TreeModifier', 'MasterDataSvc', 'FileUploader', 'Goods', '$state',
  function ($scope, $window, $timeout, $stateParams, $location, $filter, Authentication, TreeModifier, MasterDataSvc, FileUploader, Goods, $state) {
    
    // *** Master Data Initialization *** //
    $scope.goods = {};
    $scope.goods.property = {};
    $scope.goods.property.facility = [];
    $scope.goods.property.others = [];
    $scope.categories = [];
    $scope.checkedItems = [];
    $scope.formData = [];
    $scope.interface = {};
    $scope.uploadCount = 0;
    $scope.success = false;
    $scope.error = false;
    $scope.stories = [];
    $scope.selection = {};
    $scope.details = {};
    $scope.selectedSpec = {
      apartment: false
    };
    $scope.specsTitle = false;
    // ** ============================ ** //
    

    $scope.getCheckedFacilities = function(item){
      $scope.goods.property.facility = $filter('filter')($scope.facilities, { checked: true });
    };

    $scope.getCheckedOthers = function(item){
      $scope.goods.property.others = $filter('filter')($scope.others, { checked: true });
    };

    $scope.changePeriod = function(period){
      $scope.rentalPeriod = period;
    };

    $scope.checkSpecification = function(){
      $scope.populatedIds = [];
      if ($scope.goods.selectedCategories.length > 0){
        for (var idx in $scope.goods.selectedCategories){
          $scope.populatedIds.push($scope.goods.selectedCategories[idx]._id);
        }
        if ($scope.populatedIds.length > 0){
          if ($scope.populatedIds.indexOf(1212) === -1){
            $scope.selectedSpec.apartment = false;
            $scope.specsTitle = false;
          } else if ($scope.populatedIds.indexOf(1212) === 0){
            $scope.selectedSpec.apartment = true;
            $scope.specsTitle = true;
          }
        } 
      } else if ($scope.goods.selectedCategories.length === 0){
        $scope.selectedSpec.apartment = false;
        $scope.specsTitle = false;
      }
    };

    $scope.range = function(min, max, step) {
      step = step || 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
        input.push(i);
        if (i === max){
          input.push('more than ' + i);
        }
      }
      return input;
    };

    // Listen for when the interface has been configured.
    $scope.$on('$dropletReady', function whenDropletReady() {
      $scope.interface.allowedExtensions(['png', 'jpg', 'bmp', 'gif', 'svg', 'torrent']);
      $scope.interface.setRequestUrl('/api/upload/photoData');
      $scope.interface.defineHTTPSuccess([/2.{2}/]);
      $scope.interface.useArray(false);
    });
    
    // Listen for when the files have been successfully uploaded.
    $scope.$on('$dropletSuccess', function onDropletSuccess(event, response, files) {
      $scope.uploadCount = files.length;
      $scope.success = true;
      console.log(response, files);
      $timeout(function timeout() {
        $scope.success = false;
      }, 5000);
    });
    
    // Listen for when the files have failed to upload.
    $scope.$on('$dropletError', function onDropletError(event, response) {
      $scope.error = true;
      console.log(response);
      $timeout(function timeout() {
        $scope.error = false;
      }, 5000);
    });

    $scope.init = function(){

      //Categories
      MasterDataSvc.get('CATEGORIES').list(function(results, headers){
        $scope.categories = TreeModifier.listToTree(results, {
          idKey: '_id',
          parentKey: 'parentId',
          childrenKey: 'children'
        });
      });

      MasterDataSvc.get('PROPERTY_TYPES').list(function(results, headers){
        $scope.propertyTypes = results;
      });

      MasterDataSvc.get('FURNISHED').list(function(results, headers){
        $scope.furnished = results;
      });

      MasterDataSvc.get('OTHERS').list(function(results, headers){
        $scope.others = results;
        $scope.others.forEach(function(item){
          item.checked = false;
        });
      });

      MasterDataSvc.get('RENTAL_PERIOD').list(function(results, headers){
        $scope.rentalPeriods = results;
        if ($scope.rentalPeriods.length > 0)
          $scope.rentalPeriod = $scope.rentalPeriods[0];
      });

      MasterDataSvc.get('FACILITY').list(function(results, headers){
        $scope.facilities = results;
        $scope.facilities.forEach(function(item){
          item.checked = false;
        });
      });

    };
    
    //Create New Goods Item
    $scope.createGoods = function() {
      var goods = {
        category: JSON.stringify($scope.goods.selectedCategories),
        itemName: $scope.goods.itemName,
        property: JSON.stringify($scope.goods.property),
        totalPeriod: $scope.goods.totalPeriod,
        rentalPeriod: $scope.goods.rentalPeriod,
        rentalDeposit: $scope.goods.deposit,
        otherInfo: $scope.goods.otherInfo,
        user: JSON.stringify(Authentication.user)
      };

      $scope.interface.setPostData(goods);
      $scope.interface.uploadFiles();
      $state.go('goods-user-profile', { user: Authentication.user });
    };

  }
]);
'use strict';

//Goods List Controller
angular.module('goods').controller('GoodsListController', ['$scope', 'States', 'Districts', 'SubCategories', 'Goods', 'MasterDataSvc', 'TreeModifier', 'ivhTreeviewBfs',
	function($scope, States, Districts, SubCategories, Goods, MasterDataSvc, TreeModifier, ivhTreeviewBfs){
  //master data initialization
  $scope.states=[];
  $scope.districts=[];
  $scope.categories=[];
  $scope.subCategories=[];
  $scope.items=[];

  /*$('.tree-toggle').click(function () { 
    $(this).parent().children('ul.tree').toggle(200);
  });
  $(function(){
    $('.tree-toggle').parent().children('ul.tree').toggle(200);
  });*/

  $scope.showChilds = function(category){
    category.active = !category.active;
  };

  $scope.find = function(){

    //Categories
    MasterDataSvc.get('CATEGORIES').list(function(results, headers){
      $scope.categories = TreeModifier.listToTree(results, {
        idKey: '_id',
        parentKey: 'parentId',
        childrenKey: 'children'
      });
      $scope.categories.forEach(function(item){
        item.selected = false;
      });
    });
    
    MasterDataSvc.get('STATES').list(function(results, headers){
      $scope.states = results;
    });

    SubCategories.listSubCategories(function(results, headers){
      $scope.subCategories=results;
    });

    Goods.getUserGoodsList(1, 10).list(function(results, headers){
      $scope.goodsUserPaginated = results.docs;
      $scope.range(results.pages);
    });

  };

  //range
  $scope.range = function(number){
    if (number >= 1){
      $scope.numberInRange = [];
      for (var i = 1; i <= number; i++){
        $scope.numberInRange.push(i);
      }
    }
  };

  $scope.getPaginatedGoods = function(number){
    $scope.goodsUserPaginated = {};
    Goods.getUserGoodsList(number, 10).list(function(results, headers){
      $scope.goodsUserPaginated = results.docs;
      $scope.range(results.pages);
    });
  };

  $scope.CustomCallback = function (item, selectedItems) {
    if (selectedItems !== undefined && selectedItems.length >= 80) {
      return false;
    } else {
      return true;
    }
  };

  $scope.changeCallback = function(node){
    var selectedNodes = [];
    console.log('node : ' + JSON.stringify(node));
    ivhTreeviewBfs($scope.categories, function(node) {
      if(node.isSelected) {
        selectedNodes.push(node);
      }
    });
  };
}]);
'use strict';

angular.module('goods').controller('GoodsUserProfileController', ['Authentication', '$scope', 'Goods', function(Authentication,$scope,Goods){

  //Initialized Params
  $scope.user = Authentication.user;
  $scope.imageURL = $scope.user.profileImageURL;
  $scope.name = $scope.user.displayName;
  $scope.address = $scope.user.address;
  $scope.phoneNumber = $scope.user.phoneNumber;

  //init
  Goods.getUserGoodsList(1, 10).list(function(results, headers){
    console.log('results : ' + JSON.stringify(results));
    $scope.goodsUserPaginated = results.docs;
    $scope.range(results.pages);
  });

  //range
  $scope.range = function(number){
    if (number >= 1){
      $scope.numberInRange = [];
      for (var i = 1; i <= number; i++){
        $scope.numberInRange.push(i);
      }
    }
  };

}]);
(function(){
  'use strict';
  angular.module('goods')
	.service('goodsService', ['$scope', function($scope){
		
	}]);

})();
'use strict';

//States service used for communicating with the articles REST endpoints
angular.module('goods').factory('States', ['$resource',
  function ($resource) {
    return $resource('api/states', null, {
      listStates: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);

//Districts service used for communicating with the articles REST endpoints
angular.module('goods').factory('Districts', ['$resource',
  function ($resource) {
    return $resource('api/districts', null, {
      listDistricts: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);

//Categories service used for communicating with the articles REST endpoints
/*angular.module('goods').factory('Categories', ['$resource',
  function ($resource) {
    return $resource('api/categories', null, {
      listCategories: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);*/

//SubCategories service used for communicating with the articles REST endpoints
angular.module('goods').factory('SubCategories', ['$resource',
  function ($resource) {
    return $resource('api/subcategories', null, {
      listSubCategories: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);

//Goods service used for communicating with the articles REST endpoints
angular.module('goods').factory('Goods', ['$resource',
  function ($resource) {

    var goods = {};

    goods.updateById = function() {
      return $resource('api/goods/:goodId', {
        goodId: '@_id'
      }, {
        update: {
          method: 'PUT'
        }
      });
    };

    goods.getUserGoodsList = function(pageNo, limit){
      return $resource('api/goods/user?pageNo=' + pageNo + '&limit=' + limit, null, {
        list: {
          method: 'GET'
        }
      });
    };

    goods.getConstructed = function(){
      return $resource('api/goods', null, {
        listGoods: {
          method: 'GET'
        }
      });
    };

    goods.getGoodsListWithParam = function(params, pageNo, limit){
      return $resource('api/goods/param?params=' + params + 'pageNo=' + pageNo + '&limit=' + limit, null, {
        list: {
          method: 'GET'
        }
      });
    };

    return goods;
  }
]);

//SubCategories service used for communicating with the articles REST endpoints
angular.module('goods').factory('GoodList', ['$resource',
  function ($resource) {
    return $resource('api/goods', null, {
      listGoods: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);
(function() {
  'use strict';

  // Master data module config
  angular
    .module('master-data')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    // ...
  }
})();

(function () {
  'use strict';

  //Setting up route
  angular
    .module('master-data')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Master data state routing
    $stateProvider
      .state('master-data', {
        url: '/master-data',
        templateUrl: 'modules/master-data/client/views/master-data.client.view.html',
        controller: 'MasterdataController',
        controllerAs: 'vm'
      });
  }
})();

(function() {
  'use strict';

  angular
    .module('master-data')
    .controller('MasterDataController', MasterDataController);

  MasterDataController.$inject = ['$scope'];

  function MasterDataController($scope) {
    var vm = this;

    // Master data controller logic
    // ...

    init();

    function init() {
    }
  }
})();

'use strict';

//Categories factory
angular.module('master-data').factory('MasterDataTestSvc', ['$resource', '$http',
  function ($resource,$http) {

    var masterData = {};

    masterData.getCategories = function(){
      var categories = [];
      return $http({
        method: 'GET',
        url: 'api/categories',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      });
      /*$http.get('api/categories').success(function(data){
		  console.log('categories ok : ' + JSON.stringify(data));
		  categories = data;
	  });

	  return categories;*/
    };

    return masterData;
  }
]);
'use strict';

angular.module('master-data')
	.service('categoriesSvc', ['$scope', 'Categories', function($scope, Categories){
  
  $scope.getCategoriesInTree = function(){
    Categories.listCategories(function(results, headers){
      listToTree(results, {
        idKey: '_id',
        parentKey: 'parentId',
        childrenKey: 'childrenId'
      });
    });
  };

  function listToTree(data, options){
    options = options || {};
    var ID_KEY = options.idKey || '_id';
    var PARENT_KEY = options.parentKey || 'parent';
    var CHILDREN_KEY = options.parentKey || 'children';
    var item, id, parentId;
    var map = {};
    for(var idx = 0; idx < data.length; idx++){ 
      if(data[idx][ID_KEY]){
        map[data[idx][ID_KEY]] = data[idx];
        data[idx][CHILDREN_KEY] = [];
      }
    }
    for (var i = 0; i < data.length; i++) {
      if(data[i][PARENT_KEY]) { // is a child
        if(map[data[i][PARENT_KEY]]) // for dirty data
        {
          map[data[i][PARENT_KEY]][CHILDREN_KEY].push(data[i]); // add child to parent
          data.splice(i, 1); // remove from root
          i--; // iterator correction
        } else {
          data[i][PARENT_KEY] = 0; // clean dirty data
        }
      }
    }
    
    return data;
  }

}]);
'use strict';

angular.module('master-data').factory('MasterDataSvc', ['$http', '$resource',
function($http,$resource){
  
  var masterData = {};
  var sep = '\\';
  var dirMain = 'config' + sep + 'init' + sep + 'reference_data';

  masterData.get = function(filename){
    return $resource('init/reference_data/' + filename + '.json', null, {
      list: {
        method: 'GET',
        isArray: true
      }
    });
  };
  
  /*masterData.get = function(filename){
    var req = {
      method: 'GET',
      url: 'init/reference_data/' + filename + '.json',
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return $http(req);
  };*/

  return masterData;
}]);
'use strict';

//Categories factory
angular.module('shared').factory('TreeModifier', ['$resource', '$http',
function ($resource,$http) {
  var tree = {};

  tree.listToTree = function(data, options){
    options = options || {};
    var ID_KEY = options.idKey || '_id';
    var PARENT_KEY = options.parentKey || 'parentId';
    var CHILDREN_KEY = options.childrenKey || 'children';
    var item, id, parentId;
    var map = {};
    for(var j = 0; j < data.length; j++) { 
      if(data[j][ID_KEY]){
        map[data[j][ID_KEY]] = data[j];
        data[j][CHILDREN_KEY] = [];
      }
    }
    for (var i = 0; i < data.length; i++) {
      if(data[i][PARENT_KEY]) { // is a child
        if(map[data[i][PARENT_KEY]]) // for dirty data
        {
          map[data[i][PARENT_KEY]][CHILDREN_KEY].push(data[i]); // add child to parent
          data.splice(i, 1); // remove from root
          i--; // iterator correction
        } else {
          data[i][PARENT_KEY] = 0; // clean dirty data
        }
      }
    }
    return data;
  };

  return tree;
}]);
'use strict';

// Configuring the Articles module
angular.module('users.admin').run(['Menus',
  function (Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Users',
      state: 'admin.users'
    });
  }
]);

'use strict';

// Setting up route
angular.module('users.admin.routes').config(['$stateProvider',
  function ($stateProvider) {
    $stateProvider
      .state('admin.users', {
        url: '/users',
        templateUrl: 'modules/users/client/views/admin/list-users.client.view.html',
        controller: 'UserListController'
      })
      .state('admin.user', {
        url: '/users/:userId',
        templateUrl: 'modules/users/client/views/admin/view-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        }
      })
      .state('admin.user-edit', {
        url: '/users/:userId/edit',
        templateUrl: 'modules/users/client/views/admin/edit-user.client.view.html',
        controller: 'UserController',
        resolve: {
          userResolve: ['$stateParams', 'Admin', function ($stateParams, Admin) {
            return Admin.get({
              userId: $stateParams.userId
            });
          }]
        }
      });
  }
]);

'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
  function ($httpProvider) {
    // Set the httpProvider "not authorized" interceptor
    $httpProvider.interceptors.push(['$q', '$location', 'Authentication',
      function ($q, $location, Authentication) {
        return {
          responseError: function (rejection) {
            switch (rejection.status) {
              case 401:
                // Deauthenticate the global user
                Authentication.user = null;

                // Redirect to signin page
                $location.path('signin');
                break;
              case 403:
                // Add unauthorized behaviour
                break;
            }

            return $q.reject(rejection);
          }
        };
      }
    ]);
  }
]);

'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      .state('settings', {
        abstract: true,
        url: '/settings',
        templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('settings.profile', {
        url: '/profile',
        templateUrl: 'modules/users/client/views/settings/edit-profile.client.view.html'
      })
      .state('settings.password', {
        url: '/password',
        templateUrl: 'modules/users/client/views/settings/change-password.client.view.html'
      })
      .state('settings.accounts', {
        url: '/accounts',
        templateUrl: 'modules/users/client/views/settings/manage-social-accounts.client.view.html'
      })
      .state('settings.picture', {
        url: '/picture',
        templateUrl: 'modules/users/client/views/settings/change-profile-picture.client.view.html'
      })
      .state('authentication', {
        abstract: true,
        url: '/authentication',
        templateUrl: 'modules/users/client/views/authentication/authentication.client.view.html'
      })
      .state('authentication.signup', {
        url: '/signup',
        templateUrl: 'modules/users/client/views/authentication/signup.client.view.html'
      })
      .state('authentication.signin', {
        url: '/signin?err',
        templateUrl: 'modules/users/client/views/authentication/signin.client.view.html'
      })
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html'
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html'
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html'
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'modules/users/client/views/password/reset-password.client.view.html'
      });
  }
]);

'use strict';

angular.module('users.admin').controller('UserListController', ['$scope', '$filter', 'Admin',
  function ($scope, $filter, Admin) {
    Admin.query(function (data) {
      $scope.users = data;
      $scope.buildPager();
    });

    $scope.buildPager = function () {
      $scope.pagedItems = [];
      $scope.itemsPerPage = 15;
      $scope.currentPage = 1;
      $scope.figureOutItemsToDisplay();
    };

    $scope.figureOutItemsToDisplay = function () {
      $scope.filteredItems = $filter('filter')($scope.users, {
        $: $scope.search
      });
      $scope.filterLength = $scope.filteredItems.length;
      var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
      var end = begin + $scope.itemsPerPage;
      $scope.pagedItems = $scope.filteredItems.slice(begin, end);
    };

    $scope.pageChanged = function () {
      $scope.figureOutItemsToDisplay();
    };
  }
]);

'use strict';

angular.module('users.admin').controller('UserController', ['$scope', '$state', 'Authentication', 'userResolve',
  function ($scope, $state, Authentication, userResolve) {
    $scope.authentication = Authentication;
    $scope.user = userResolve;

    $scope.remove = function (user) {
      if (confirm('Are you sure you want to delete this user?')) {
        if (user) {
          user.$remove();

          $scope.users.splice($scope.users.indexOf(user), 1);
        } else {
          $scope.user.$remove(function () {
            $state.go('admin.users');
          });
        }
      }
    };

    $scope.update = function (isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      var user = $scope.user;

      user.$update(function () {
        $state.go('admin.user', {
          userId: user._id
        });
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$state', '$http', '$location', '$window', 'Authentication', 'PasswordValidator',
  function ($scope, $state, $http, $location, $window, Authentication, PasswordValidator) {
    $scope.authentication = Authentication;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();
    //$scope.credentials = {};

    // Get an eventual error defined in the URL query string:
    $scope.error = $location.search().err;

    // If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    /*//initialize value
    $scope.credentials.businessOwner = "professional";*/

    $scope.signup = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signup', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;

        // And redirect to the previous or home page
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    $scope.signin = function (isValid) {
      $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      $http.post('/api/auth/signin', $scope.credentials).success(function (response) {
        // If successful we assign the response to the global user model
        $scope.authentication.user = response;

        // And redirect to the previous or home page
        $state.go($state.previous.state.name || 'home', $state.previous.params);
      }).error(function (response) {
        $scope.error = response.message;
      });
    };

    // OAuth provider request
    $scope.callOauthProvider = function (url) {
      if ($state.previous && $state.previous.href) {
        url += '?redirect_to=' + encodeURIComponent($state.previous.href);
      }

      // Effectively call OAuth authentication route:
      $window.location.href = url;
    };
  }
]);

'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication', 'PasswordValidator',
  function ($scope, $stateParams, $http, $location, Authentication, PasswordValidator) {
    $scope.authentication = Authentication;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    //If user is signed in then redirect back home
    if ($scope.authentication.user) {
      $location.path('/');
    }

    // Submit forgotten password account id
    $scope.askForPasswordReset = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'forgotPasswordForm');

        return false;
      }

      $http.post('/api/auth/forgot', $scope.credentials).success(function (response) {
        // Show user success message and clear form
        $scope.credentials = null;
        $scope.success = response.message;

      }).error(function (response) {
        // Show user error message and clear form
        $scope.credentials = null;
        $scope.error = response.message;
      });
    };

    // Change user password
    $scope.resetUserPassword = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'resetPasswordForm');

        return false;
      }

      $http.post('/api/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.passwordDetails = null;

        // Attach user profile
        Authentication.user = response;

        // And redirect to the index page
        $location.path('/password/reset/success');
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', 'Authentication', 'PasswordValidator',
  function ($scope, $http, Authentication, PasswordValidator) {
    $scope.user = Authentication.user;
    $scope.popoverMsg = PasswordValidator.getPopoverMsg();

    // Change user password
    $scope.changeUserPassword = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'passwordForm');

        return false;
      }

      $http.post('/api/users/password', $scope.passwordDetails).success(function (response) {
        // If successful show success message and clear form
        $scope.$broadcast('show-errors-reset', 'passwordForm');
        $scope.success = true;
        $scope.passwordDetails = null;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('ChangeProfilePictureController', ['$scope', '$timeout', '$window', 'Authentication', 'FileUploader',
  function ($scope, $timeout, $window, Authentication, FileUploader) {
    $scope.user = Authentication.user;
    $scope.imageURL = $scope.user.profileImageURL;

    // Create file uploader instance
    $scope.uploader = new FileUploader({
      url: 'api/users/picture',
      alias: 'newProfilePicture'
    });

    // Set file uploader image filter
    $scope.uploader.filters.push({
      name: 'imageFilter',
      fn: function (item, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // Called after the user selected a new picture file
    $scope.uploader.onAfterAddingFile = function (fileItem) {
      if ($window.FileReader) {
        var fileReader = new FileReader();
        fileReader.readAsDataURL(fileItem._file);

        fileReader.onload = function (fileReaderEvent) {
          $timeout(function () {
            $scope.imageURL = fileReaderEvent.target.result;
          }, 0);
        };
      }
    };

    // Called after the user has successfully uploaded a new picture
    $scope.uploader.onSuccessItem = function (fileItem, response, status, headers) {
      // Show success message
      $scope.success = true;

      // Populate user object
      $scope.user = Authentication.user = response;

      // Clear upload buttons
      $scope.cancelUpload();
    };

    // Called after the user has failed to uploaded a new picture
    $scope.uploader.onErrorItem = function (fileItem, response, status, headers) {
      // Clear upload buttons
      $scope.cancelUpload();

      // Show error message
      $scope.error = response.message;
    };

    // Change user profile picture
    $scope.uploadProfilePicture = function () {
      // Clear messages
      $scope.success = $scope.error = null;

      // Start upload
      $scope.uploader.uploadAll();
    };

    // Cancel the upload process
    $scope.cancelUpload = function () {
      $scope.uploader.clearQueue();
      $scope.imageURL = $scope.user.profileImageURL;
    };
  }
]);

'use strict';

angular.module('users').controller('EditProfileController', ['$scope', '$http', '$location', 'Users', 'Authentication',
  function ($scope, $http, $location, Users, Authentication) {
    $scope.user = Authentication.user;

    // Update a user profile
    $scope.updateUserProfile = function (isValid) {
      $scope.success = $scope.error = null;

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');

        return false;
      }

      var user = new Users($scope.user);

      user.$update(function (response) {
        $scope.$broadcast('show-errors-reset', 'userForm');

        $scope.success = true;
        Authentication.user = response;
      }, function (response) {
        $scope.error = response.data.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('SocialAccountsController', ['$scope', '$http', 'Authentication',
  function ($scope, $http, Authentication) {
    $scope.user = Authentication.user;

    // Check if there are additional accounts
    $scope.hasConnectedAdditionalSocialAccounts = function (provider) {
      for (var i in $scope.user.additionalProvidersData) {
        return true;
      }

      return false;
    };

    // Check if provider is already in use with current user
    $scope.isConnectedSocialAccount = function (provider) {
      return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
    };

    // Remove a user social account
    $scope.removeUserSocialAccount = function (provider) {
      $scope.success = $scope.error = null;

      $http.delete('/api/users/accounts', {
        params: {
          provider: provider
        }
      }).success(function (response) {
        // If successful show success message and clear form
        $scope.success = true;
        $scope.user = Authentication.user = response;
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

'use strict';

angular.module('users').controller('SettingsController', ['$scope', 'Authentication',
  function ($scope, Authentication) {
    $scope.user = Authentication.user;
  }
]);

'use strict';

angular.module('users')
  .directive('passwordValidator', ['PasswordValidator', function(PasswordValidator) {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$validators.requirements = function (password) {
          var status = true;
          if (password) {
            var result = PasswordValidator.getResult(password);
            var requirementsIdx = 0;

            // Requirements Meter - visual indicator for users
            var requirementsMeter = [
              { color: 'danger', progress: '20' },
              { color: 'warning', progress: '40' },
              { color: 'info', progress: '60' },
              { color: 'primary', progress: '80' },
              { color: 'success', progress: '100' }
            ];

            if (result.errors.length < requirementsMeter.length) {
              requirementsIdx = requirementsMeter.length - result.errors.length - 1;
            }

            scope.requirementsColor = requirementsMeter[requirementsIdx].color;
            scope.requirementsProgress = requirementsMeter[requirementsIdx].progress;

            if (result.errors.length) {
              scope.popoverMsg = PasswordValidator.getPopoverMsg();
              scope.passwordErrors = result.errors;
              status = false;
            } else {
              scope.popoverMsg = '';
              scope.passwordErrors = [];
              status = true;
            }
          }
          return status;
        };
      }
    };
  }]);

'use strict';

angular.module('users')
  .directive('passwordVerify', [function() {
    return {
      require: 'ngModel',
      scope: {
        passwordVerify: '='
      },
      link: function(scope, element, attrs, ngModel) {
        var status = true;
        scope.$watch(function() {
          var combined;
          if (scope.passwordVerify || ngModel) {
            combined = scope.passwordVerify + '_' + ngModel;
          }
          return combined;
        }, function(value) {
          if (value) {
            ngModel.$validators.passwordVerify = function (password) {
              var origin = scope.passwordVerify;
              return (origin !== password) ? false : true;
            };
          }
        });
      }
    };
  }]);

'use strict';

// Users directive used to force lowercase input
angular.module('users').directive('lowercase', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push(function (input) {
        return input ? input.toLowerCase() : '';
      });
      element.css('text-transform', 'lowercase');
    }
  };
});

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', ['$window',
  function ($window) {
    var auth = {
      user: $window.user
    };

    return auth;
  }
]);

'use strict';

// PasswordValidator service used for testing the password strength
angular.module('users').factory('PasswordValidator', ['$window',
  function ($window) {
    var owaspPasswordStrengthTest = $window.owaspPasswordStrengthTest;

    return {
      getResult: function (password) {
        var result = owaspPasswordStrengthTest.test(password);
        return result;
      },
      getPopoverMsg: function () {
        var popoverMsg = 'Please enter a passphrase or password with greater than 10 characters, numbers, lowercase, upppercase, and special characters.';
        return popoverMsg;
      }
    };
  }
]);

'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
  function ($resource) {
    return $resource('api/users', {}, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

//TODO this should be Users service
angular.module('users.admin').factory('Admin', ['$resource',
  function ($resource) {
    return $resource('api/users/:userId', {
      userId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
