'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.css',
        'public/lib/bootstrap/dist/css/bootstrap-theme.css',
        'public/lib/css/my-rent-style.css',
        'public/lib/css/owl.carousel.css',
        'public/lib/css/owl.theme.css',
        'public/lib/angular-multi-select-alexandernst/dist/prod/angular-multi-select.min.css',
        'public/lib/kjvelarde-angular-multiselectsearchtree/dist/kjvelarde-multiselect-searchtree.min.css',
        'public/lib/angular-ivh-treeview/dist/ivh-treeview.min.css'
      ],
      js: [
        'public/lib/jquery/dist/jquery.min.js',
        'public/lib/plugins/jquery.fs.scroller/jquery.fs.scroller.js',
        'public/lib/plugins/jquery.fs.selecter/jquery.fs.selecter.js',
        'public/lib/plugins/autocomplete/jquery.mockjax.js',
        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-messages/angular-messages.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.js',
        'public/lib/bootstrap/dist/js/bootstrap.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/lib/angular-file-upload/angular-file-upload.min.js',
        'public/lib/angular-ivh-treeview/dist/ivh-treeview.min.js',
        'public/lib/lokijs/src/lokijs.js',
        'public/lib/angular-multi-select-alexandernst/dist/prod/angular-multi-select.js',
        'public/lib/kjvelarde-angular-multiselectsearchtree/dist/kjvelarde-multiselect-searchtree.min.js',
        'public/lib/kjvelarde-angular-multiselectsearchtree/dist/kjvelarde-multiselect-searchtree.tpl.js',
        'public/lib/ng-droplet/dist/ng-droplet.js',
        'public/lib/owasp-password-strength-test/owasp-password-strength-test.js',
        'public/lib/js/pace.min.js',
        'public/lib/js/owl.carousel.min.js',
        'public/lib/js/form-validation.js',
        'public/lib/js/jquery.matchHeight-min.js',
        'public/lib/js/hideMaxListItem.js',
        'public/lib/plugins/autocomplete/usastates.js',
        'public/lib/plugins/jquery.fs.scroller/jquery.fs.scroller.min.js',
        'public/lib/plugins/jquery.fs.selecter/jquery.fs.selecter.min.js',
        'public/lib/js/script.js'
      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/*/client/css/*.css'
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: 'gruntfile.js',
    gulpConfig: 'gulpfile.js',
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: 'modules/*/server/config/*.js',
    policies: 'modules/*/server/policies/*.js',
    views: 'modules/*/server/views/*.html'
  }
};
