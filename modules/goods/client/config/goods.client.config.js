(function() {
  'use strict';

  // Goods module config
  angular
    .module('goods')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    // ...
  }
})();
