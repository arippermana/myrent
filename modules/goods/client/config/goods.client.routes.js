(function () {
  'use strict';

  //Setting up route
  angular
    .module('goods')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Goods state routing
    $stateProvider
      .state('goods-create', {
        url: '/goodsCreate',
        templateUrl: 'modules/goods/client/views/good-create.client.view.html',
        controller: 'GoodsCreateController',
        controllerAs: 'vm'
      })
      .state('good-list', {
        url: '/goodList',
        templateUrl: 'modules/goods/client/views/good-list.client.view.html',
        controller: 'GoodsListController',
        ControllerAs: 'vm'
      })
      .state('good-test', {
        url: '/goodsTest',
        templateUrl: 'modules/goods/client/views/goods-test-upload.client.view.html',
        ControllerAs: 'vm'
      })
      .state('goods-test-dragdrop', {
        url: '/goodsTestDragDrop',
        templateUrl: 'modules/goods/client/views/goods.test-droplet.client.view.html',
        controller: 'IndexController',
        controllerAs: 'vm'
      })
      .state('goods-user-profile',{
        url: '/goodsUserProfile',
        templateUrl: 'modules/goods/client/views/goods-seller-profile.client.view.html',
        controller: 'GoodsUserProfileController',
        params: {
          user: ''
        }
      });
  }
})();
