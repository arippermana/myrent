'use strict';

//States service used for communicating with the articles REST endpoints
angular.module('goods').factory('States', ['$resource',
  function ($resource) {
    return $resource('api/states', null, {
      listStates: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);

//Districts service used for communicating with the articles REST endpoints
angular.module('goods').factory('Districts', ['$resource',
  function ($resource) {
    return $resource('api/districts', null, {
      listDistricts: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);

//Categories service used for communicating with the articles REST endpoints
/*angular.module('goods').factory('Categories', ['$resource',
  function ($resource) {
    return $resource('api/categories', null, {
      listCategories: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);*/

//SubCategories service used for communicating with the articles REST endpoints
angular.module('goods').factory('SubCategories', ['$resource',
  function ($resource) {
    return $resource('api/subcategories', null, {
      listSubCategories: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);

//Goods service used for communicating with the articles REST endpoints
angular.module('goods').factory('Goods', ['$resource',
  function ($resource) {

    var goods = {};

    goods.updateById = function() {
      return $resource('api/goods/:goodId', {
        goodId: '@_id'
      }, {
        update: {
          method: 'PUT'
        }
      });
    };

    goods.getUserGoodsList = function(pageNo, limit){
      return $resource('api/goods/user?pageNo=' + pageNo + '&limit=' + limit, null, {
        list: {
          method: 'GET'
        }
      });
    };

    goods.getConstructed = function(){
      return $resource('api/goods', null, {
        listGoods: {
          method: 'GET'
        }
      });
    };

    goods.getGoodsListWithParam = function(params, pageNo, limit){
      return $resource('api/goods/param?params=' + params + 'pageNo=' + pageNo + '&limit=' + limit, null, {
        list: {
          method: 'GET'
        }
      });
    };

    return goods;
  }
]);

//SubCategories service used for communicating with the articles REST endpoints
angular.module('goods').factory('GoodList', ['$resource',
  function ($resource) {
    return $resource('api/goods', null, {
      listGoods: {
        method: 'GET',
        isArray: true
      }
    });
  }
]);