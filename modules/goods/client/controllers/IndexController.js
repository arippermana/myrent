'use strict';

//Goods List Controller
angular.module('goods').controller('IndexController', ['$scope', 'States', 'Districts', 'Categories', 'SubCategories', 'GoodList', '$timeout',
    function($scope, States, Districts, Categories, SubCategories, GoodList, $timeout){
  //master data initialization
      $scope.interface = {};
      $scope.uploadCount = 0;
      $scope.success = false;
      $scope.error = false;

      // Listen for when the interface has been configured.
      $scope.$on('$dropletReady', function whenDropletReady() {
        $scope.interface.allowedExtensions(['png', 'jpg', 'bmp', 'gif', 'svg', 'torrent']);
        $scope.interface.setRequestUrl('upload.html');
        $scope.interface.defineHTTPSuccess([/2.{2}/]);
        $scope.interface.useArray(false);
      });
    
      // Listen for when the files have been successfully uploaded.
      $scope.$on('$dropletSuccess', function onDropletSuccess(event, response, files) {
        $scope.uploadCount = files.length;
        $scope.success = true;
        console.log(response, files);
        $timeout(function timeout() {
          $scope.success = false;
        }, 5000);
      });
    
      // Listen for when the files have failed to upload.
      $scope.$on('$dropletError', function onDropletError(event, response) {
        $scope.error = true;
        console.log(response);
        $timeout(function timeout() {
          $scope.error = false;
        }, 5000);
      });
    }]);