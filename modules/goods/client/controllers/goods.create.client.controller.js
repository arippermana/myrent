'use strict';

//Goods Controller
angular.module('goods').controller('GoodsCreateController', ['$scope', '$window', '$timeout', '$stateParams', '$location', '$filter', 'Authentication', 'TreeModifier', 'MasterDataSvc', 'FileUploader', 'Goods', '$state',
  function ($scope, $window, $timeout, $stateParams, $location, $filter, Authentication, TreeModifier, MasterDataSvc, FileUploader, Goods, $state) {
    
    // *** Master Data Initialization *** //
    $scope.goods = {};
    $scope.goods.property = {};
    $scope.goods.property.facility = [];
    $scope.goods.property.others = [];
    $scope.categories = [];
    $scope.checkedItems = [];
    $scope.formData = [];
    $scope.interface = {};
    $scope.uploadCount = 0;
    $scope.success = false;
    $scope.error = false;
    $scope.stories = [];
    $scope.selection = {};
    $scope.details = {};
    $scope.selectedSpec = {
      apartment: false
    };
    $scope.specsTitle = false;
    // ** ============================ ** //
    

    $scope.getCheckedFacilities = function(item){
      $scope.goods.property.facility = $filter('filter')($scope.facilities, { checked: true });
    };

    $scope.getCheckedOthers = function(item){
      $scope.goods.property.others = $filter('filter')($scope.others, { checked: true });
    };

    $scope.changePeriod = function(period){
      $scope.rentalPeriod = period;
    };

    $scope.checkSpecification = function(){
      $scope.populatedIds = [];
      if ($scope.goods.selectedCategories.length > 0){
        for (var idx in $scope.goods.selectedCategories){
          $scope.populatedIds.push($scope.goods.selectedCategories[idx]._id);
        }
        if ($scope.populatedIds.length > 0){
          if ($scope.populatedIds.indexOf(1212) === -1){
            $scope.selectedSpec.apartment = false;
            $scope.specsTitle = false;
          } else if ($scope.populatedIds.indexOf(1212) === 0){
            $scope.selectedSpec.apartment = true;
            $scope.specsTitle = true;
          }
        } 
      } else if ($scope.goods.selectedCategories.length === 0){
        $scope.selectedSpec.apartment = false;
        $scope.specsTitle = false;
      }
    };

    $scope.range = function(min, max, step) {
      step = step || 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
        input.push(i);
        if (i === max){
          input.push('more than ' + i);
        }
      }
      return input;
    };

    // Listen for when the interface has been configured.
    $scope.$on('$dropletReady', function whenDropletReady() {
      $scope.interface.allowedExtensions(['png', 'jpg', 'bmp', 'gif', 'svg', 'torrent']);
      $scope.interface.setRequestUrl('/api/upload/photoData');
      $scope.interface.defineHTTPSuccess([/2.{2}/]);
      $scope.interface.useArray(false);
    });
    
    // Listen for when the files have been successfully uploaded.
    $scope.$on('$dropletSuccess', function onDropletSuccess(event, response, files) {
      $scope.uploadCount = files.length;
      $scope.success = true;
      console.log(response, files);
      $timeout(function timeout() {
        $scope.success = false;
      }, 5000);
    });
    
    // Listen for when the files have failed to upload.
    $scope.$on('$dropletError', function onDropletError(event, response) {
      $scope.error = true;
      console.log(response);
      $timeout(function timeout() {
        $scope.error = false;
      }, 5000);
    });

    $scope.init = function(){

      //Categories
      MasterDataSvc.get('CATEGORIES').list(function(results, headers){
        $scope.categories = TreeModifier.listToTree(results, {
          idKey: '_id',
          parentKey: 'parentId',
          childrenKey: 'children'
        });
      });

      MasterDataSvc.get('PROPERTY_TYPES').list(function(results, headers){
        $scope.propertyTypes = results;
      });

      MasterDataSvc.get('FURNISHED').list(function(results, headers){
        $scope.furnished = results;
      });

      MasterDataSvc.get('OTHERS').list(function(results, headers){
        $scope.others = results;
        $scope.others.forEach(function(item){
          item.checked = false;
        });
      });

      MasterDataSvc.get('RENTAL_PERIOD').list(function(results, headers){
        $scope.rentalPeriods = results;
        if ($scope.rentalPeriods.length > 0)
          $scope.rentalPeriod = $scope.rentalPeriods[0];
      });

      MasterDataSvc.get('FACILITY').list(function(results, headers){
        $scope.facilities = results;
        $scope.facilities.forEach(function(item){
          item.checked = false;
        });
      });

    };
    
    //Create New Goods Item
    $scope.createGoods = function() {
      var goods = {
        category: JSON.stringify($scope.goods.selectedCategories),
        itemName: $scope.goods.itemName,
        property: JSON.stringify($scope.goods.property),
        totalPeriod: $scope.goods.totalPeriod,
        rentalPeriod: $scope.goods.rentalPeriod,
        rentalDeposit: $scope.goods.deposit,
        otherInfo: $scope.goods.otherInfo,
        user: JSON.stringify(Authentication.user)
      };

      $scope.interface.setPostData(goods);
      $scope.interface.uploadFiles();
      $state.go('goods-user-profile', { user: Authentication.user });
    };

  }
]);