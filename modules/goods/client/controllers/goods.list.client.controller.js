'use strict';

//Goods List Controller
angular.module('goods').controller('GoodsListController', ['$scope', 'States', 'Districts', 'SubCategories', 'Goods', 'MasterDataSvc', 'TreeModifier', 'ivhTreeviewBfs',
	function($scope, States, Districts, SubCategories, Goods, MasterDataSvc, TreeModifier, ivhTreeviewBfs){
  //master data initialization
  $scope.states=[];
  $scope.districts=[];
  $scope.categories=[];
  $scope.subCategories=[];
  $scope.items=[];

  /*$('.tree-toggle').click(function () { 
    $(this).parent().children('ul.tree').toggle(200);
  });
  $(function(){
    $('.tree-toggle').parent().children('ul.tree').toggle(200);
  });*/

  $scope.showChilds = function(category){
    category.active = !category.active;
  };

  $scope.find = function(){

    //Categories
    MasterDataSvc.get('CATEGORIES').list(function(results, headers){
      $scope.categories = TreeModifier.listToTree(results, {
        idKey: '_id',
        parentKey: 'parentId',
        childrenKey: 'children'
      });
      $scope.categories.forEach(function(item){
        item.selected = false;
      });
    });
    
    MasterDataSvc.get('STATES').list(function(results, headers){
      $scope.states = results;
    });

    SubCategories.listSubCategories(function(results, headers){
      $scope.subCategories=results;
    });

    Goods.getUserGoodsList(1, 10).list(function(results, headers){
      $scope.goodsUserPaginated = results.docs;
      $scope.range(results.pages);
    });

  };

  //range
  $scope.range = function(number){
    if (number >= 1){
      $scope.numberInRange = [];
      for (var i = 1; i <= number; i++){
        $scope.numberInRange.push(i);
      }
    }
  };

  $scope.getPaginatedGoods = function(number){
    $scope.goodsUserPaginated = {};
    Goods.getUserGoodsList(number, 10).list(function(results, headers){
      $scope.goodsUserPaginated = results.docs;
      $scope.range(results.pages);
    });
  };

  $scope.CustomCallback = function (item, selectedItems) {
    if (selectedItems !== undefined && selectedItems.length >= 80) {
      return false;
    } else {
      return true;
    }
  };

  $scope.changeCallback = function(node){
    var selectedNodes = [];
    console.log('node : ' + JSON.stringify(node));
    ivhTreeviewBfs($scope.categories, function(node) {
      if(node.isSelected) {
        selectedNodes.push(node);
      }
    });
  };
}]);