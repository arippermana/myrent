'use strict';

angular.module('goods').controller('GoodsUserProfileController', ['Authentication', '$scope', 'Goods', function(Authentication,$scope,Goods){

  //Initialized Params
  $scope.user = Authentication.user;
  $scope.imageURL = $scope.user.profileImageURL;
  $scope.name = $scope.user.displayName;
  $scope.address = $scope.user.address;
  $scope.phoneNumber = $scope.user.phoneNumber;

  //init
  Goods.getUserGoodsList(1, 10).list(function(results, headers){
    console.log('results : ' + JSON.stringify(results));
    $scope.goodsUserPaginated = results.docs;
    $scope.range(results.pages);
  });

  //range
  $scope.range = function(number){
    if (number >= 1){
      $scope.numberInRange = [];
      for (var i = 1; i <= number; i++){
        $scope.numberInRange.push(i);
      }
    }
  };

}]);