'use strict';

//Goods Controller
angular.module('goods').controller('GoodsController', ['$scope', '$stateParams', '$location', 'Authentication', 'States', 'Districts', 'Categories', 'SubCategories', 'Goods',
  function ($scope, $stateParams, $location, Authentication, States, Districts, Categories, SubCategories, Goods) {
    //Master Data Initialization
    $scope.states = [];
    $scope.districts = [];
    $scope.categories = [];
    $scope.subcategories = [];

    $scope.find = function(){
      //States
      States.listStates(function(results, headers) {
        $scope.states = results;
      });

      //Districts
      Districts.listDistricts(function(results, headers){
        $scope.districts = results;
      });

      //Categories
      Categories.listCategories(function(results, headers){
        var categories = results;
        
      });

      //SubCategories
      SubCategories.listSubCategories(function(results, headers){
        $scope.subcategories = results;
      });
    };

    // Create new Good
    $scope.createGood = function () {
      $scope.error = null;

      /*if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'articleForm');

        return false;
      }*/

      // Create new Article object
      var good = new Goods({
        category: this.good.category,
        subCategory: this.good.subCategory,
        adType: this.good.type,
        adTitle: this.good.title,
        adDesc: this.good.description,
        price: this.good.price,
        state: this.good.state,
        district: this.good.district
      });

      // Redirect after save
      good.$save(function (response) {
        $location.path('articles/' + response._id);

        // Clear form fields
        //$scope.title = '';
        //$scope.content = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }
]);