'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  mongoosePaginate = require('mongoose-paginate');

/**
 * Article Schema
 */
var GoodSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  /*category: {
    type: Schema.Types.Mixed,
    ref: 'Category'
  },*/
  itemName: {
    type: String
  },
  category: [
    {
      _id: Number,
      parentId: String,
      text: String,
      order: Number,
      name: String,
      level: Number,
      open: Boolean,
      checked: Boolean
    }
  ],
  goodsImageUrl: [
    {
      name: String
    }
  ],
  specification: {
    propertyType: {
      name: String,
      id: Number,
      order: Number,
      group: String
    },
    bedrooms: Number,
    bathrooms: Number,
    size: Number,
    furnished: {
      name: String,
      id: Number,
      order: Number,
      group: String
    },
    facility:[
      {
        name: String,
        id: Number,
        order: Number,
        group: String
      }
    ],
    others:[
      {
        name: String,
        id: Number,
        order: Number,
        group: String
      }
    ]
  },
  totalPeriod: {
    type: Number
  },
  rentalPeriod: {
    name: String,
    id: Number,
    order: Number,
    group: String
  },
  rentalDeposit: {
    type: Number
  },
  otherInfo: {
    type: String
  },
  user: {
    type: Schema.Types.Mixed,
    ref: 'User'
  }
});

GoodSchema.plugin(mongoosePaginate);
mongoose.model('Good', GoodSchema);
