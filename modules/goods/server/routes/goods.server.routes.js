'use strict';

/**
 * Module dependencies.
 */
var goodsPolicy = require('../policies/goods.server.policy'),
  goods = require('../controllers/goods.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/goods').all(goodsPolicy.isAllowed)
    .post(goods.create);
  app.route('/api/goods/picture1').post(goods.uploadGoodPicture);
  app.route('/api/goods')
  	.get(goods.list);
  app.route('/api/goodsupload')
    .post(goods.createWithUpload);
  app.route('/api/upload/photoData')
  	.post(goods.goodsUpload);
  app.route('/api/goods/user')
    .get(goods.userGoodsList);
  app.route('/api/goods/param')
    .get(goods.goodsListwithParameter);
};
