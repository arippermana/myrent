'use strict';

/**
 * Module dependencies.
 */

var path = require('path'),
  mongoose = require('mongoose'),
  Good = mongoose.model('Good'),
  multer = require('multer'),
  fs = require('fs'),
  config = require(path.resolve('./config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

 /**
 * Create a goods with photo
 */
exports.createWithUpload = function(req, res){
  var file = req.files.file;
  var goodsReq = JSON.parse(req.body.good);
  var good = new Good(goodsReq);
  good.user = req.user;

  fs.readFile(file.path, function(err, original_data){
    if(err){
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    }

    var base64Image = original_data.toString('base64');
    fs.unlink(file.path, function(err){
      if(err){
        console.log('failed to delete');
      } else {
        console.log('successfully delete');
      }
    });

    good.goodFirstImageUrl = base64Image;
    good.save(function(err){
      if (err){
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(good);
      }
    });
  });
};

 /**
 * Create a goods
 */
exports.create = function (req, res) {
  console.log('checking dulu');
  var good = new Good(req.body);
  good.user = req.user;

  good.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(good);
    }
  });
};

/**
 * Update a goods
 */
exports.update = function (req, res) {
  var good = req.good;

  good.category = req.body.category;
  good.subCategory = req.body.subCategory;
  good.adType = req.body.adType;
  good.adTitle = req.body.adTitle;
  good.adDesc = req.body.adDesc;
  good.price = req.body.price;
  good.state = req.body.state;
  good.district = req.body.district;
  good.goodFirstImageUrl = req.body.goodFirstImageUrl;
  good.user = req.body.user;

  good.save(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(good);
    }
  });
};

/**
 * Delete a goods
 */
exports.delete = function (req, res) {
  var good = req.article;

  good.remove(function (err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(good);
    }
  });
};

/**
*  Update goods picture
*/
exports.uploadGoodPicture = function(req, res){
  //var forms = formidable.IncomingForm();
  //forms.parse(req, function(err, maxFields){
  //  console.log('apa saja ok : ' + JSON.stringify(maxFields));
  //});
  console.log('is ok : ' + JSON.stringify(req.body));
  var good = req.body.formData;
  good.user = req.user;
  var message = null;
  var upload = multer(config.uploads.goodsUpload).single('goodsPicture');
  var goodUploadFileFilter = require(path.resolve('./config/lib/multer')).imageUploadFileFilter;

  // Filtering to upload only images
  upload.fileFilter = goodUploadFileFilter;

  if (good) {
    console.log('mulai upload');
    upload(req, res, function (uploadError) {
      if(uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading profile picture'
        });
      } else {
        good.goodFirstImageUrl = config.uploads.goodsUpload.dest + req.file.filename;

        good.save(function (saveError) {
          if (saveError) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(saveError)
            });
          } else {
            res.json(good);
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'Good is not found'
    });
  }
};

/**
 * List of Goods
 */
exports.list = function (req, res) {
  Good.find().exec(function (err, goods) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(goods);
    }
  });
};

exports.testingParam = function(req, res, next, id){
  res.json({ id: id });
};

exports.goodsListwithParameter = function(req, res){
  var paramsCategory = [];
  for (var idx in req.query.params.category){
    paramsCategory.push(req.query.params.category[idx].text);
  }
  var query = { 'category': { $elemMatch: { 'text': 'Apartment' } } };
  var options = {
    page: Number(req.query.pageNo),
    limit: Number(req.query.limit)
  };
  Good.paginate(query, options, function(error, results){
    if (error){
      console.log(error);
    } else {
      console.log('Results : ', results);
      res.json(results);
    }
  });
};

exports.userGoodsList = function(req, res){
  /*Good.find({user: req.user}).exec(function(err, goods){
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(goods);
    }
  });*/
  console.log('page no = ' + req.query.pageNo);
  console.log('limit = ' + req.query.limit);
  var query = { 'user.displayName': req.user.displayName };
  var options = {
    page: Number(req.query.pageNo),
    limit: Number(req.query.limit)
  };
  Good.paginate(query, options, function(error, results) {
    if (error) {
      console.error(error);
    } else {
      console.log('Results : ', results);
      res.json(results);
    }
  });
};

exports.userGoodsListPagination = function(req, res, next, value) {
  console.log(value);
  res.json(value);
};

exports.goodsUpload = function(req, res){
  var message = null;
  var uploadImage = multer(config.uploads.goodsUpload).array('file');
  var goodUploadFileFilterTest = require(path.resolve('./config/lib/multer')).imageUploadFileFilter;
  // Filtering to upload only images
  uploadImage.fileFilter = goodUploadFileFilterTest;
  uploadImage(req, res, (function(uploadError){
    if (uploadError){
      return res.status(400).send({
        message: errorHandler.getErrorMessage('uploading is error')
      });
    } else {
      var count = req.files.length;
      var files = [];
      for (var idx in req.files){
        files.push({
          name: config.uploads.goodsUpload.dest + req.files[idx].filename
        });
      }
      var goods = new Good({
        category: JSON.parse(req.body.category),
        itemName: req.body.itemName,
        specification: JSON.parse(req.body.property),
        totalPeriod: req.body.totalPeriod,
        rentalPeriod: req.body.rentalPeriod,
        rentalDeposit: req.body.deposit,
        otherInfo: req.body.otherInfo,
        goodsImageUrl: files,
        user: req.user
      });

      goods.save(function(saveError){
        if (saveError){
          return res.status(400).send({
            message: errorHandler.getErrorMessage('can not save goods')
          });
        } else {
          res.json(goods);
        }
      });
    }
  }));
};
