(function () {
  'use strict';

  angular
    .module('chat')
    .factory('chatService', chatService);

  chatService.$inject = [/*Example: '$state', '$window' */];

  function chatService(/*Example: $state, $window */) {
    // Chatservice service logic
    // ...

    // Public API
    return {
      someMethod: function () {
        return true;
      }
    };
  }

  function doSomething(){
    console.log('hi arip');
  }
})();
