'use strict';

angular.module('core').controller('HomeController', ['$scope', 'Authentication', 'States',
  function ($scope, Authentication, States) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope.states=[];

    $scope.stateSearchResult = function(){
      return $scope.states;
    };

    $scope.find = function(){
      //States
      States.listStates(function(results, headers) {
        $scope.states = results;
      });
    };
  }
]);
