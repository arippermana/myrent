'use strict';

//Categories factory
angular.module('master-data').factory('MasterDataTestSvc', ['$resource', '$http',
  function ($resource,$http) {

    var masterData = {};

    masterData.getCategories = function(){
      var categories = [];
      return $http({
        method: 'GET',
        url: 'api/categories',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        }
      });
      /*$http.get('api/categories').success(function(data){
		  console.log('categories ok : ' + JSON.stringify(data));
		  categories = data;
	  });

	  return categories;*/
    };

    return masterData;
  }
]);