(function () {
  'use strict';

  //Setting up route
  angular
    .module('master-data')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    // Master data state routing
    $stateProvider
      .state('master-data', {
        url: '/master-data',
        templateUrl: 'modules/master-data/client/views/master-data.client.view.html',
        controller: 'MasterdataController',
        controllerAs: 'vm'
      });
  }
})();
