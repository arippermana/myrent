(function() {
  'use strict';

  // Master data module config
  angular
    .module('master-data')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(Menus) {
    // Config logic
    // ...
  }
})();
