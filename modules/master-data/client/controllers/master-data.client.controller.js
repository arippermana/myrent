(function() {
  'use strict';

  angular
    .module('master-data')
    .controller('MasterDataController', MasterDataController);

  MasterDataController.$inject = ['$scope'];

  function MasterDataController($scope) {
    var vm = this;

    // Master data controller logic
    // ...

    init();

    function init() {
    }
  }
})();
