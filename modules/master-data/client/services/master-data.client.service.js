'use strict';

angular.module('master-data').factory('MasterDataSvc', ['$http', '$resource',
function($http,$resource){
  
  var masterData = {};
  var sep = '\\';
  var dirMain = 'config' + sep + 'init' + sep + 'reference_data';

  masterData.get = function(filename){
    return $resource('init/reference_data/' + filename + '.json', null, {
      list: {
        method: 'GET',
        isArray: true
      }
    });
  };
  
  /*masterData.get = function(filename){
    var req = {
      method: 'GET',
      url: 'init/reference_data/' + filename + '.json',
      headers: {
        'Content-Type': 'application/json'
      }
    };

    return $http(req);
  };*/

  return masterData;
}]);