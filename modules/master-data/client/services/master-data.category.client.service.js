'use strict';

angular.module('master-data')
	.service('categoriesSvc', ['$scope', 'Categories', function($scope, Categories){
  
  $scope.getCategoriesInTree = function(){
    Categories.listCategories(function(results, headers){
      listToTree(results, {
        idKey: '_id',
        parentKey: 'parentId',
        childrenKey: 'childrenId'
      });
    });
  };

  function listToTree(data, options){
    options = options || {};
    var ID_KEY = options.idKey || '_id';
    var PARENT_KEY = options.parentKey || 'parent';
    var CHILDREN_KEY = options.parentKey || 'children';
    var item, id, parentId;
    var map = {};
    for(var idx = 0; idx < data.length; idx++){ 
      if(data[idx][ID_KEY]){
        map[data[idx][ID_KEY]] = data[idx];
        data[idx][CHILDREN_KEY] = [];
      }
    }
    for (var i = 0; i < data.length; i++) {
      if(data[i][PARENT_KEY]) { // is a child
        if(map[data[i][PARENT_KEY]]) // for dirty data
        {
          map[data[i][PARENT_KEY]][CHILDREN_KEY].push(data[i]); // add child to parent
          data.splice(i, 1); // remove from root
          i--; // iterator correction
        } else {
          data[i][PARENT_KEY] = 0; // clean dirty data
        }
      }
    }
    
    return data;
  }

}]);