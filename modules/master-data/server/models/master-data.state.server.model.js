'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * State Schema
 */
var stateSchema = new Schema({
  stateName: {
    type: String,
    default: '',
    trim: true,
    required: 'State Name cannot be blank'
  },
  stateId: {
    type: Number,
    default: 0
  },
  stateCode: {
    type: String,
    default: '',
    trim: true,
    required: 'State Code cannot be blank'
  }
});

mongoose.model('state', stateSchema);
