'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * State Schema
 */
var categorySchema = new Schema({
  _id: {
    type: Number,
    default: 0
  },
  parentId: {
    type: String
  },
  level: {
    type: Number,
    default: 0
  },
  name: {
    type: String,
    default: ''
  },
  order: {
    type: Number,
    default: 0
  },
  text: {
    type: String
  },
  open: {
    type: Boolean
  }, 
  checked: {
    type: Boolean
  }
});

mongoose.model('category', categorySchema);
