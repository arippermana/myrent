'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * State Schema
 */
var subCategorySchema = new Schema({
  categoryId: {
    type: Number,
    default: 0
  },
  subCategoryId: {
    type: Number,
    default: 0
  },
  subCategoryName: {
    type: String,
    default: '',
    trim: true,
    required: 'Sub Category Name cannot be blank'
  },
  status: {
    type: Boolean,
    default: true
  },
  subCategoryCode: {
    type: String,
    default: '',
    trim: true,
    required: 'Sub Category Code cannot be blank'
  }
});

mongoose.model('subCategory', subCategorySchema);
