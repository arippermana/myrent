'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * State Schema
 */
var districtSchema = new Schema({
  districtId: {
    type: Number,
    default: 0
  },
  districtName: {
    type: String,
    default: '',
    trim: true,
    required: 'District Name cannot be blank'
  },
  stateId: {
    type: Number,
    default: 0
  },
  districtCode: {
    type: String,
    default: '',
    trim: true,
    required: 'District Code cannot be blank'
  }
});

mongoose.model('district', districtSchema);
