'use strict';

/**
 * Module dependencies.
 */
var statesPolicy = require('../policies/master-data.states.server.policy'),
  states = require('../controllers/master-data.states.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/states').all(statesPolicy.isAllowed)
    .get(states.list);
};
