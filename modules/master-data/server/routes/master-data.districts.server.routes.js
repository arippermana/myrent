'use strict';

/**
 * Module dependencies.
 */
var statesPolicy = require('../policies/master-data.states.server.policy'),
  districts = require('../controllers/master-data.districts.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/districts')
    .get(districts.list);
};
