'use strict';

/**
 * Module dependencies.
 */
var subCategories = require('../controllers/master-data.subCategories.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/subCategories')
    .get(subCategories.list);
};
