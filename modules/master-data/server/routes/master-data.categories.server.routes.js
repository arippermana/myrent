'use strict';

/**
 * Module dependencies.
 */
var categories = require('../controllers/master-data.categories.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/categories')
    .get(categories.list);
};
