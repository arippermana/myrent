'use strict';

//Categories factory
angular.module('shared').factory('TreeModifier', ['$resource', '$http',
function ($resource,$http) {
  var tree = {};

  tree.listToTree = function(data, options){
    options = options || {};
    var ID_KEY = options.idKey || '_id';
    var PARENT_KEY = options.parentKey || 'parentId';
    var CHILDREN_KEY = options.childrenKey || 'children';
    var item, id, parentId;
    var map = {};
    for(var j = 0; j < data.length; j++) { 
      if(data[j][ID_KEY]){
        map[data[j][ID_KEY]] = data[j];
        data[j][CHILDREN_KEY] = [];
      }
    }
    for (var i = 0; i < data.length; i++) {
      if(data[i][PARENT_KEY]) { // is a child
        if(map[data[i][PARENT_KEY]]) // for dirty data
        {
          map[data[i][PARENT_KEY]][CHILDREN_KEY].push(data[i]); // add child to parent
          data.splice(i, 1); // remove from root
          i--; // iterator correction
        } else {
          data[i][PARENT_KEY] = 0; // clean dirty data
        }
      }
    }
    return data;
  };

  return tree;
}]);